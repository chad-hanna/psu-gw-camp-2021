## Stuff from "The basics of LIGO data" day 2


## Random collection of stuff from mcdemo

- [x] - What is a GPS time? [Covered in day 2 "The basics of..."](https://docs.google.com/presentation/d/1nOO8kWcVTIb7Tfb4MIIGODXyzTCK_ZVFLf2mVZ3qMjM/edit#slide=id.ge0272d0858_0_31)
- [ ] - What is a "detector" (define H1, L1, V1)?
- [ ] - What is GWOSC?
- [x] - What is strain and/or strain data? [Covered in day 2 "The basics of..."](https://docs.google.com/presentation/d/1nOO8kWcVTIb7Tfb4MIIGODXyzTCK_ZVFLf2mVZ3qMjM/edit#slide=id.ge0272d0858_0_31)
- [x] - What is a time-series? [Covered in day 2 "The basics of..."](https://docs.google.com/presentation/d/1nOO8kWcVTIb7Tfb4MIIGODXyzTCK_ZVFLf2mVZ3qMjM/edit#slide=id.ge0272d0858_0_31)
- [x] - What are the basic properties of waves? (Amplitude, frequency, period, etc. Define jargon like Cycles.) [Covered in day 2 "The basics of..."](https://docs.google.com/presentation/d/1nOO8kWcVTIb7Tfb4MIIGODXyzTCK_ZVFLf2mVZ3qMjM/edit#slide=id.ge0272d0858_0_31)
- [x] - What is noise and how does one characterize the frequency content of noise? [Covered in day 2 "The basics of..."](https://docs.google.com/presentation/d/1nOO8kWcVTIb7Tfb4MIIGODXyzTCK_ZVFLf2mVZ3qMjM/edit#slide=id.ge0272d0858_0_31)
- [x] - What is filtering? (band-pass) [Covered in day 2 "The basics of..."](https://docs.google.com/presentation/d/1nOO8kWcVTIb7Tfb4MIIGODXyzTCK_ZVFLf2mVZ3qMjM/edit#slide=id.ge0272d0858_0_31)
- [x] - What are the time-domain and frequency domain properties of a GW? [Covered in day 3 "GW sourcess..."](https://docs.google.com/presentation/d/1tg_09AYNl9vz8RppuHpzg5WBA12RY_wdewXnI4sZ7qA/edit#slide=id.ge0a0802cf6_0_94)
- [x] - What is the coalescence time, merger frequency, etc of a GW? [Covered in day 3 "GW sourcess..."](https://docs.google.com/presentation/d/1tg_09AYNl9vz8RppuHpzg5WBA12RY_wdewXnI4sZ7qA/edit#slide=id.ge0a0802cf6_0_94)
- [ ] - What is a log plot, or what is the value of looking at the logarithm of given data?
- [ ] - What is data calibration?
- [x] - What is a power-spectrum or an amplitude spectrum? [Covered in day 2 "The basics of..."](https://docs.google.com/presentation/d/1nOO8kWcVTIb7Tfb4MIIGODXyzTCK_ZVFLf2mVZ3qMjM/edit#slide=id.ge0272d0858_0_31)
- [x] - What is chirp mass? [Covered in day 3 "GW sourcess..."](https://docs.google.com/presentation/d/1tg_09AYNl9vz8RppuHpzg5WBA12RY_wdewXnI4sZ7qA/edit#slide=id.ge0a0802cf6_0_94)
- [ ] - What is a spectrogram?


## Excercises that are part of talks (not the jupyter demos)

## Day 2

1. Estimate the probability that a GW is present in LIGO data
2. Describe noise as red or white


## Day 3

1. Compute power lost in the Earth-Sun orbit due to GW
2. Code a simple waveform model and learn about how parameters change the waveform
3. Using the plots of various events made on day 2, try to estimate some basic properties learned from exercise 2



