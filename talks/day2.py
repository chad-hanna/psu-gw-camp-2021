#!/usr/bin/env python
from matplotlib import pyplot
import numpy
import scipy
numpy.random.seed(7)
t = numpy.linspace(0,1,1000)
f = numpy.linspace(0,1000,1000)
a = 1e-22 * numpy.sin(100 * t)
s = 1e-22 * numpy.sin(2 * numpy.pi * 50 * t)
n = 1e-22 * numpy.random.randn(len(t))
redfilt = 200 * (t-0.5)**6
red = numpy.real(scipy.ifft(redfilt * scipy.fft(n)))
d = n + s

fd = scipy.fft(d)
fd[:45] = 0.0
fd[55:500] = 0.
fd[-500:-55] = 0.
fd[-45:] = 0.
fd = numpy.real(scipy.ifft(fd))
    
if 0:
    fig = pyplot.figure(figsize=(8,3))
    pyplot.plot(t,a)
    pyplot.xlabel('time (s)')
    pyplot.ylabel('strain')
    fig.tight_layout()
    pyplot.show()
    
    fig = pyplot.figure(figsize=(8,3))
    pyplot.plot(t,a, '.', markersize=8)
    pyplot.xlabel('time (s)')
    pyplot.ylabel('strain')
    fig.tight_layout()
    pyplot.show()
    
    fig = pyplot.figure(figsize=(8,3))
    pyplot.plot(t, n, '.-', markersize=8)
    pyplot.xlabel('time (s)')
    pyplot.ylabel('strain')
    fig.tight_layout()
    pyplot.show()
    
    fig = pyplot.figure(figsize=(8,7))
    pyplot.subplot(211)
    pyplot.plot(t, red, '.-', markersize=8)
    pyplot.subplot(212)
    pyplot.plot(t, n, '.-', markersize=8)
    pyplot.xlabel('time (s)')
    pyplot.ylabel('strain / Hz^(1/2)')
    fig.tight_layout()
    pyplot.show()
    
    fig = pyplot.figure(figsize=(8,7))
    pyplot.subplot(211)
    pyplot.plot(f[:len(f)//2], numpy.abs(scipy.fft(red))[:len(f)//2], '.-', markersize=8)
    pyplot.subplot(212)
    pyplot.plot(f[:len(f)//2], numpy.abs(scipy.fft(n))[:len(f)//2], '.-', markersize=8)
    pyplot.xlabel('freq (Hz)')
    pyplot.ylabel('strain / Hz^(1/2)')
    fig.tight_layout()
    pyplot.show()

fig = pyplot.figure(figsize=(8,3))
pyplot.plot(t, d, '.-', markersize=8)
pyplot.xlim([0,0.2])
pyplot.xlabel('time (s)')
pyplot.ylabel('strain')
fig.tight_layout()
pyplot.show()
    
fig = pyplot.figure(figsize=(8,3))
pyplot.plot(t, d, '.-', markersize=8)
pyplot.plot(t, s, '.-', markersize=8, alpha=0.4)
pyplot.xlabel('time (s)')
pyplot.ylabel('strain')
pyplot.xlim([0,0.2])
fig.tight_layout()
pyplot.show()
    
fig = pyplot.figure(figsize=(8,3))
pyplot.plot(f[:len(f)//2], numpy.abs(scipy.fft(d))[:len(f)//2], '.-', markersize=8)
pyplot.xlabel('freq (Hz)')
pyplot.ylabel('strain / Hz^(1/2)')
fig.tight_layout()
pyplot.show()

fig = pyplot.figure(figsize=(8,3))
pyplot.plot(f[:len(f)//2], numpy.abs(scipy.fft(d))[:len(f)//2], '.-', markersize=8)
pyplot.plot(f[:len(f)//2], numpy.abs(scipy.fft(s))[:len(f)//2], '.-', markersize=8, alpha=0.4)
pyplot.xlabel('freq (Hz)')
pyplot.ylabel('strain / Hz^(1/2)')
fig.tight_layout()
pyplot.show()

fig = pyplot.figure(figsize=(8,3))
pyplot.plot(f[:len(f)//2], numpy.abs(scipy.fft(fd))[:len(f)//2], '.-', markersize=8)
pyplot.plot(f[:len(f)//2], numpy.abs(scipy.fft(s))[:len(f)//2], '.-', markersize=8, alpha=0.4)
pyplot.xlabel('freq (Hz)')
pyplot.ylabel('strain / Hz^(1/2)')
fig.tight_layout()
pyplot.show()

fig = pyplot.figure(figsize=(8,3))
pyplot.plot(t, fd, '.-', markersize=8)
pyplot.plot(t, s, '.-', markersize=8, alpha=0.4)
pyplot.xlabel('time (s)')
pyplot.ylabel('strain')
pyplot.xlim([0,0.2])
fig.tight_layout()
pyplot.show()

