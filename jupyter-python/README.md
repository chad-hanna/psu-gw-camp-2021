# Jupyter Notes

- **URL**:  https://jupyter.gwave.psu.edu
- **Login**: Use your PSU Access ID and password. This requires a normal or sponsored PSU Access account.

## The Launcher window

When you open Jupyter, you will see the Launcher window. It has three rows. The top row launches new Jupyter notebook windows. The middle row launches Python console sessions. The bottom has a `Terminal` button for launching Linux command-line terminals. For the purposes of the camp, you can ignore the other buttons on the third row. The Launcher window looks like:

![Launcher window](images/launcher_window.png "Jupyter Launcher window")


## The File Browser window

The File Browser window shows you files in your *home directory* on Jupyer. It looks something like this:

![File Browser window](images/file_browser_window.png "Jupyter File Browser window")

The file browser is usually on the left side of the Jupyter window. It may be hidden. If it is, just click on the folder icon on the left side of the Jupyter window to show it.

It is important to remember that the files that it shows you are *on the Jupyter server* and **NOT** on your local computer. 

## Jupyter notebook environments

The only Python environment that you should use is:  **igwn-38**. On the Jupyter Launcher window, it will be labeled: `Python [conda env:igwn-38]`.

This environment has all of the Python modules required for working through the examples for the camp. If you use any other environment, you will see errors about missing modules while working through examples. If you open up a notebook with another environment, it's easy to fix. In the upper right-hand corner of the notebook window you can see the current environment in use. If you click it, you will get a dialog where you can select `Python [conda env:igwn-38]`.

A Jupyter notebook window looks like:

![Jupyter notebook window](images/jupyter_notebook_window.png "Jupyter notebook window")

Note the environment, `Python [conda env:igwn-38]`, in the upper-right corner of the window.

## Terminal access

The Terminal access buttons open a web terminal (or *command-line*) session.

Once you click on the `Terminal` button, you will see a Linux command line prompt. At the prompt you can type linux shell commands. Don't worry if you've never used a Linux command line before. Your instructors will tell you what you need to type.

A terminal window looks something like:

![Terminal  window](images/terminal_window.png "Jupyter Terminal window")

The string `(base) [rdt12@psuligo-03 ~]$` is different for every user. It is called a *prompt* and is the place where you begin entering commands. After you type a command, any command output is displayed and you will see a new prompt.

Commands that you run are executed on the Jupyter server, **NOT** on your local machine.

The shell does not act on commands until you press the `Enter` key. A useful command is `ls`. It prints the files in your current working directory.


## Downloading files from the web.

You can download files from the web using the the `curl` command in a terminal window. For example, the command:

```bash
    curl https://colab.research.google.com/github/losc-tutorial/quickview/blob/master/index.ipynb
```
will download a file named `index.ipynb` into your home directory. If you already have a file name `index.ipynb` and don't want to overwrite it, you can add `-o output.ipynb` to the command. For example, the command:

```bash
    curl https://colab.research.google.com/github/losc-tutorial/quickview/blob/master/index.ipynb -o foo.ipynb
```

will download the same file, but save it as `foo.ipynb`.

After a minute or so, any file that you download should appear in the File Browser of your Jupyter window. You can force Jupyter to update its list by pressing the refresh button on the File Browser. It looks like a circular arrow.

Your instructors will tell you what to download if you need to download files.

## Downloading git repositories

Git is a set of programs used to share files and keep track of changes in files. A *git repository* is a set of files that can be downloaded using `git`.  Your instructors may ask you to download a git repository. For example, the git repository:

   https://github.com/losc-tutorial/quickview

can be download using the following `git` comamand in the terminal window:

```bash
  https://github.com/losc-tutorial/quickview
```

This will create a directory named `quickview` in your current working directory.

Like files downloaded with `curl`, the directory will show up in your File Browser window after a minute or so.

Your instructors will let you know if you need to download any git repositories.

## Copying files from the `/shared` directory.

Your instructor may place files under the `/shared` directory on the Jupyter server and ask you to copy them to your
home directory. To see the files in the `/shared` directory you can use the `ls` command in a Terminal window ilke so:

```bash
  ls /shared
```

You will see at least the file `foo.txt` there. You can copy it to your current working directory using the command `cp` like so:

```bash
  cp /shared/foo.txt  foo.txt
```

Notice that the `cp` command takes two arguments, the file to copy and where to copy it. In this case, the file is copied to `foo.txt` in your current working directory.

Some other useful commands:

- `file foo.txt`
- `cat foo.txt`
- `less foo.txt`

The `file` command tells you the type of file (text, binary, etc.) The cat command prints the entire file. You should only do this on short text files. The `less` command prints the entire file a page at a time. If you run `less`, you can press the `spacebar` to go to the next page and `q` to quit/end the `less` program.

Your instructor will have you use one of the following commands to copy folders into your home directory at the beginning of each day. To use a command, paste it into your terminal and press Enter.

**Day 1:** `cp -r /shared/psu-gw-camp-2021/jupyter-python/day1 ~`

**Day 2:** `cp -r /shared/psu-gw-camp-2021/jupyter-python/day2 ~`

**Day 3:** `cp -r /shared/psu-gw-camp-2021/jupyter-python/day3 ~`

## Disconnect dialog

There is a bug or misfeature in Jupyterlab, the software that presents the Jupyter website. Very frequently it will pop up a a dialog box that says, "Server Not Running". You can safely click `Dismiss` when this dialog pops up.




