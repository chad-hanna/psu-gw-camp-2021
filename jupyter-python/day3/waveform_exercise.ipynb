{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gravitational Wave sources, modeling and waveforms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This IPython notebook has some activities from the lecture on sources of gravitational waves, and how we model them. \n",
    "\n",
    "We will first look at how much energy is lost in GWs, specifically for the Earth-Sun system. \n",
    "\n",
    "Then, we will move onto modelling the gravitational waveform for a binary system of compact objects. We will define some python functions for the waveform, then further see how the masses of objects and the distance to the source affect the waveform. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's import some packages that we'll need as we move forward in the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "import matplotlib\n",
    "from matplotlib import pyplot\n",
    "import scipy\n",
    "\n",
    "%config InlineBackend.figure_format = 'retina'\n",
    "\n",
    "matplotlib.rcParams['figure.figsize'] = [8,6]\n",
    "matplotlib.rcParams['font.size'] = 15\n",
    "matplotlib.rcParams['legend.fontsize'] = 12"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Exercise 1: Energy lost in gravitational waves\n",
    "\n",
    "The power lost in gravitational waves as two objects orbit each other is given by\n",
    "\n",
    "\\begin{equation}\n",
    "    \\frac{dE}{dt} = P = \\frac{32}{5} \\frac{G^4}{c^5} \\frac{m_1^2 m_2^2 (m_1 + m_2)}{r^5}\n",
    "\\end{equation}\n",
    "\n",
    "Here $G = 6.7 \\times 10^{-11} {\\rm m^3 kg^{-1} s^{-2}}$ is the **Gravitational constant** and $c =  3 \\times 10^8 {\\rm m s^{-1}}$ is the **speed of light in vacuum**. \n",
    "\n",
    "Find the power lost in gravitational waves over time for the Earth-Sun orbit. Use $r = 1.5 \\times 10^{11} {\\rm m}$, $m_{\\rm sun} = 2 \\times 10^{30} {\\rm kg}$, $m_{\\rm earth} = 6 \\times 10^{24} {\\rm kg}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 1:** Define constants."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Constants\n",
    "G = 6.7e-11\n",
    "c = 3e8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 2:** Write power as a function of the component masses and distance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def power(m1, m2, r):\n",
    "    ## Your code here ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 3:** Compute the power for the Earth-Sun orbit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your code here ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 2: Visualising the Gravitational waveform\n",
    "\n",
    "The gravitational waveform can be modelled as\n",
    "\n",
    "\\begin{equation}\n",
    "    h_+(t) = 4 \\frac{\\mathcal{M}_s}{d_{\\rm L}}(\\pi \\mathcal{M}_s f)^{2/3} cos \\left[ -2 \\left( \\frac{T-t}{5\\mathcal{M}_s}\\right)^{5/8}\\right]\n",
    "\\end{equation}\n",
    "\n",
    "where,\n",
    "\n",
    "\\begin{eqnarray}\n",
    "    f = \\frac{1}{\\pi \\mathcal{M}_s} \\left[\\frac{5}{256} \\frac{\\mathcal{M}_s}{T - t}\\right]^{3/8} \\\\\n",
    "    \\mathcal{M}_s = \\mathcal{M} \\left( \\frac{G}{c^3}\\right) \\\\\n",
    "    \\mathcal{M} = \\frac{(m_1m_2)^{3/5}}{(m_1 + m_2)^{1/5}} \\\\\n",
    "    d_{\\rm L} = d/c\n",
    "\\end{eqnarray}\n",
    "\n",
    "\n",
    "Here, $\\mathcal{M}$ is the chirp mass of a binary system. We use this combination of masses because this is how they appear in a lot of our calculations. \n",
    "\n",
    "We will be working in units where $G=c=1$. So, masses and distances will be used in units of seconds. $\\mathcal{M}_s$ is just a unit conversion step where we use a combination of $G$ and $c$ to convert kilograms to seconds. Similarly, we convert the units of distance $d$ from metres to seconds by dividing it by the speed of light to get $d_{\\rm L}$ in seconds.   \n",
    "\n",
    "The merger frequency for a coalescing binary can be approximated as \n",
    "\n",
    "\\begin{equation}\n",
    "    f_{\\rm merger} = \\frac{7000 {\\rm Hz}}{(m_1 + m_2)/M_\\odot}\n",
    "\\end{equation}\n",
    "\n",
    "#### We will now plot the gravitational waveform for different systems and see how mass and distance affects the waveform."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 0. Define some useful constants"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Constants\n",
    "G = 6.7e-11\n",
    "c = 3e8\n",
    "Msun = 2e30 # mass of sun in Kg\n",
    "Mpc = 3e22 # Mpc in meters (Mpc is a common astronomical distance measure)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 1: Define some functions\n",
    "\n",
    "Write up 3 functions:\n",
    "1. `m_chirp(m1, m2)` which takes the component masses, $m_1$ and $m_2$, as inputs to compute the chirp mass, $\\mathcal{M}$, of the binary system. \n",
    "2. `f_merger(m1, m2)` which takes the component masses, $m_1$ and $m_2$, as inputs and returns the merger frequency, $f_{\\rm merger}$.\n",
    "3. `h(m1, m2 , d, t)` which takes the component masses, $m_1$ and $m_2$, the distance to the source, $d$, and time, $t$ as inputs to return the strain $h_+(t)$. \n",
    "\n",
    "In defining `h(m1,m2,d,t)`, keep in mind:\n",
    "- units of input quantities; units should be SI (kg, m, s, etc.)\n",
    "- Detector sensitivity; our present detectors are sensitive within some frequency band which means the strain will be zero outside this frequency band. **To account for LIGO’s frequency response, set the waveform to zero below 30 Hz. Also set the waveform to zero above the merger frequency, $f_{\\rm merger}$.**\n",
    "- Use T = 10s (for illustration)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def m_chirp(m1, m2):\n",
    "    ## Your code here ##\n",
    "\n",
    "def f_merger(m1, m2):\n",
    "    ## Your code here ##\n",
    "\n",
    "def h(m1, m2 , d, t):\n",
    "    ## Your code here ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step  2: Generate the waveform\n",
    "\n",
    "Using 100,000 samples starting at t = 0 sampled at 10,000 samples per second, generate $h_+(t)$ for $m_1 = 10 M_{\\rm sun}$ and $m_2 = 3 M_{\\rm sun}$ at $d = 25$ Mpc, and plot it. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate an time array starting at t = 0 upto t = T with 10,000 samples per second. \n",
    "\n",
    "t = ## Your code here ##"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define m1, m2, d\n",
    "m1 = 10. * Msun\n",
    "m2 = 3. * Msun\n",
    "d  = 25. * Mpc\n",
    "\n",
    "# Generate an array of h(t) for the time array, t\n",
    "h_of_t = ## Your code here ##\n",
    "\n",
    "# Plot h(t) as a function of t\n",
    "pyplot.plot(t, h_of_t)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Let's see how masses and distance affect the waveform.\n",
    "\n",
    "Visualize the waveform for different masses in the range of 1-100 $M_{\\rm sun}$ and distances d in the range of 1-1000 Mpc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1. How does it change as you vary mass?\n",
    "\n",
    "- Define a mass array ranging from 1 to 100 $M_{\\rm sun}$.\n",
    "- Fix $d = 20$ Mpc.\n",
    "- Write a for loop to input different masses from the array to compute $h_+(t)$ for the time array and plot it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define mass and distance\n",
    "mass     =\n",
    "distance =\n",
    "\n",
    "# For loop to go over masses and plotting it\n",
    "## Your code here ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Questions to ponder:\n",
    "\n",
    "1. How does the amplitude of the waveform change with varying chirp mass, $\\mathcal{M}$?\n",
    "2. How does the chirp mass, $\\mathcal{M}$ affect the duration of the GW signal?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2. How does it change as you vary distance?\n",
    "\n",
    "- Define a distance array ranging from 1 to 1000 Mpc.\n",
    "- Fix $m_1$ and $m_2$.\n",
    "- Write a for loop to input different distance values from the array to compute $h_+(t)$ for the time array and plot it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set masses and distance\n",
    "mass1 = \n",
    "mass2 = \n",
    "\n",
    "print('M_chirp: ', m_chirp(mass1, mass2))\n",
    "distances = \n",
    "\n",
    "# For loop to go over distances and plotting it\n",
    "## Your code here ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Questions to ponder:\n",
    "\n",
    "1. How does the amplitude of the waveform change with varying distance?\n",
    "2. How does the distance affect the duration of the GW signal?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. How does it change as you vary both mass and distance? Can you change the distance and mass while holding the peak amplitude constant? Can you infer the relationship?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**HINT:** Look at the dependence of $f$ on $\\mathcal{M}_s$. Plug that into $h_+(t)$ and try to simplify the equation for $h_+(t)$ to see how it depends on the combination of $\\mathcal{M}_s$ and $d_{\\rm L}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4. Compare the absolute value of Fourier Transform of $h_+(t)$ to $h^2(f) \\propto f^{-7/6}$.\n",
    "\n",
    "For a 5, 5 $M_{\\rm sun}$ binary at 1 Mpc, compute the fourier transform of $h_+(t)$ using the `scipy` function called `scipy.fft.rfft`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute h_of_t for the system\n",
    "h_of_t = \n",
    "\n",
    "# Compute fourier transform of h_of_t\n",
    "h_of_f = "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the absolute value `abs()` of the amplitude of the Fourier Transform as a function of frequency on a log-log plot. Use an x-axis array defined as `f = 1 / T * numpy.arange(len(hf))`.\n",
    "\n",
    "Compare $h(f)=3 \\times 10^{-15}f^{-7/6} $ to this result. \n",
    "\n",
    "Note on a logarithmic plot, $y=x^n$ becomes the equation for a line with slope $n$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Absolute value of the amplitude of fourier transform of h_of_t\n",
    "abs_h_of_f = \n",
    "\n",
    "# Compute frequencies\n",
    "\n",
    "#Set fmin (lowest frequency that LIGO is sensitive to) and fmax (f_merger)\n",
    "fmin = \n",
    "fmax = \n",
    "\n",
    "## Plotting\n",
    "pyplot.loglog(f, f**(-7./6.)*3e-15, label='Analytic waveform')\n",
    "pyplot.loglog(f, abs_h_of_f, label='FFT waveform')\n",
    "\n",
    "pyplot.xlim([fmin, fmax])\n",
    "\n",
    "pyplot.legend()\n",
    "pyplot.grid(True, ls='--', alpha=0.5)\n",
    "pyplot.xlabel('f (Hz)')\n",
    "pyplot.ylabel('h(f)')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
