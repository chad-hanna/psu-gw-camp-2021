{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Identifying the chirp mass of GW150914\n",
    "\n",
    "Welcome! \n",
    "This IPython notebook provides a hands on demo to measure the chirp mass of the very first gravitational wave discovery GW150914.\n",
    "\n",
    "This notebook is modified from https://colab.research.google.com/github/losc-tutorial/quickview/blob/master/index.ipynb v0.6; August 2019\n",
    "\n",
    "The solutions to this exercise can be found in the same folder as this notebook. \n",
    "We encourage you to try working it out on your own before you check the solution. \n",
    "If you get stuck, you can look up error messages, python functions, python libraries, etc. on the internet. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "#### Import some packages\n",
    "Here are some python packages that are useful for working with gravitational wave data. You can look up the individual packages on the internet to find out what each of the libraries let you do."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests, os\n",
    "import matplotlib.pyplot as plt\n",
    "%config InlineBackend.figure_format = 'retina'\n",
    "from gwpy.timeseries import TimeSeries\n",
    "import numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set the GPS time\n",
    "GPS time is used to specify what data you want to work with. Set the coalescence time (1126259462.4) as your starting GPS time (t0). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# -- Set a GPS time:\n",
    "t0 = 1126259462.4    # -- GW150914 coalescence time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set detector\n",
    "Select the detector (as H1, L1, or V1) that you want to analyze the data for. \n",
    "Hint: Choose the detector that detected the gravitational wave with the \"loudest\" signal, which in this case was H1.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#-- Choose detector as H1, L1, or V1. We will choose H1 since the signal was the \"loudest\" in H1.\n",
    "detector = 'H1'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Query and download data file\n",
    "Unless you have already, you have to import [this library](https://gwosc.readthedocs.io/en/latest/reference/gwosc.locate.get_urls.html) to get urls from GWOSC (Gravitational Wave Open Science Center).\n",
    "Use it to get the url to the data for GW150914. You can use t0 for the start and stop times in get_urls. The print statement is useful to track if your code is working. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gwosc.locate import get_urls\n",
    "url = get_urls(detector, t0, t0)[-1]\n",
    "\n",
    "print('Downloading: ' , url)\n",
    "fn = os.path.basename(url)\n",
    "with open(fn,'wb') as strainfile:                 \n",
    "    straindata = requests.get(url)\n",
    "    strainfile.write(straindata.content)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot the raw time-series data\n",
    "Now that you have the unfiltered strain data, plot it near the coalescence time to see what it looks like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# -- Read strain data\n",
    "strain = TimeSeries.read(fn,format='hdf5.losc')\n",
    "center = int(t0)\n",
    "strain = strain.crop(center-16, center+16)\n",
    "fig1 = strain.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filtering the data - first step - understand the frequency content of the signal and the noise.\n",
    "\n",
    "Notice how noisy the raw data is. Nothing here looks particularly like a gravitational wave signal. Recall, however, that it is possible to isolate important signals in the data through filtering. Here we will motivate the band pass filter appropriate for a GW.\n",
    "\n",
    "#### The frequency content of a GW signal\n",
    "To understand what might be a good filter, recall that the amplitude as a function of frequency for gravitational wave signals is well described as\n",
    "\n",
    "$|\\tilde{h}(f)| \\sim f^{-7/6}$\n",
    "\n",
    "The typical amplitude of a GW in f follows this negative power law function of $f$.  If we plot this on a logarithmically scaled axis, it will be a straight line with slope = -7/6 because: \n",
    "\n",
    "$\\ln (f^{-7/6}) = -\\frac{7}{6} \\ln (f)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------------------------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 1\n",
    "\n",
    "## Problem:\n",
    "Compare the gravitational wave frequency content to the noise frequency content.\n",
    "\n",
    "#### What we want to code: \n",
    "We want to plot the amplitude of the data, `d`, as a function of the frequency, `f`. \n",
    "The plot should have `f` as the x-values and `d` as the y-values. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 1: \n",
    "Start by assigning the data amplitude to `d`, and assigning the frequency to `f`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d = strain.asd(fftlength=8)\n",
    "f = numpy.arange(len(d)) * d.df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 2: \n",
    "Write some code that plots `d` versus `f` using a log-log plot. Use xlimits between 10 and 1500, and ylimits between 1e-24 and 1e-19. You can look up what matplotlib.pyplot (that you imported as `plt` earlier) can do. Complete the code below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.loglog(f,d)\n",
    "\n",
    "### Your code here ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 3: \n",
    "It is important for your plots to have axes labels so that others know what you have plotted. \n",
    "In the code you wrote to create a plot in Hint 2, add some code to give both x and y axes appropriate labels."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 4:\n",
    "In the code you wrote to create a plot in Hint 2, add some code to __plot the relationship of `d` and `f`, `y=5e-21 * f**(-7/6)`.__ Remember: This represents the amplitude of a typical GW, so it should add a straight line to the graph."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------------------------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Visualize the whitened and band-passed data between 20 and 200 Hz\n",
    "From the plot you have made in Exercise 1, it seems that frequencies between 20 and 200 Hz might be promising for typical gravitational waves from a binary black hole.  Next we will filter data to isolate those frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# -- Filter the data to isolate frequencies between 20 and 200 Hz\n",
    "bp_data = strain.whiten().bandpass(20, 200)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------------------------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 2\n",
    "\n",
    "## Problem: \n",
    "Visualize the portion of the data that contains the gravitational wave signal. \n",
    "\n",
    "#### What we want to code: \n",
    "Plot the data over a 100ms (0.1s) range from `t0-0.065` to `t0+0.035`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 1: \n",
    "Use the plot() method of bp_data to plot the time series between `t0-0.065` to `t0+0.035`.\n",
    "The time comes on the x-axis so use xlimits of `t0-0.065` and `t0+0.035`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Your code here ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------------------------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 3\n",
    "\n",
    "## Problem:\n",
    "Identify the gravitational wave cycles (positive peaks) from the graph.\n",
    "\n",
    "#### More details before you start working:\n",
    "We want to find trends that are generally increasing in amplitude and frequency over adjacent cycles until the peak. We expect this because merging black holes orbit faster and faster until they finally merge.\n",
    "\n",
    "#### Before you code:\n",
    "1. By-eye, start by identifying the peak with the largest positive amplitude.\n",
    "2. Go left from this point, identifying each peak until the trend is no longer decreasing over a few cycles. Don't focus on the small ups and downs, but rather the cycles that appear to go above and below y = 0.\n",
    "3. Record the time (horizontal) values of these points as t values. You should find 6 of them. Only focus on the positve y-value peaks, and ignore the negative y-value troughs. Try to estimate the time values to three decimal places (all should start with 0.) \n",
    "\n",
    "#### What we want to code:\n",
    "Store the time values of all six peaks in an array called `t`.\n",
    "Make sure your list is in order from smallest to largest value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 1:\n",
    "You can use `sorted` to make sure your list is in order from smallest to largest.\n",
    "Your list `t` should be in the format like below. (`#` is used to comment out a line, telling python to ignore it when you execute the cell)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# t = sorted([t1, t2, t3, t4, t5, t6])\n",
    "\n",
    "### Your code here ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------------------------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 4\n",
    "\n",
    "## Problem: \n",
    "Find the relationship between the instantaneous period, the time before coalescence, and the chirp mass."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Part A:\n",
    "\n",
    "#### More details before you start working:\n",
    "In order to compute the chirp mass $\\mathcal{M}$ $M_\\odot$ (the mass of our Sun), we will use this approximate relationship between the frequency of the waveform and the chirp mass:\n",
    "\n",
    "$f=\\frac{1}{\\pi \\mathcal{M} (M_\\odot G / c^3)}\\left[ \\frac{5}{256} \\frac{\\mathcal{M} (M_\\odot G / c^3)}{\\Delta t} \\right]^{3/8}$\n",
    "\n",
    "\n",
    "Where $T$ is the period of the waveform, $G = 6.7 \\times 10^{-11}$ m$^3$ kg$^{-1}$ s$^{-2}$ (the gravitational force constant), and $c = 3 \\times 10^8$ m s$^{-1}$ (the speed of light).\n",
    "\n",
    "Next we use the relationship between frequency, $f$ and period, $T$\n",
    "\n",
    "$f = 1 / T$\n",
    "\n",
    "We can then rearange the two equations above to get\n",
    "\n",
    "$\\frac{\\mathcal{M}}{ M_\\odot} = 3000 \\, T^{8/5} \\Delta t^{-3/5}$\n",
    "\n",
    "which gives the chirp mass in units of solar masses (typically used in our field)\n",
    "\n",
    "#### What we want to code (part 1):\n",
    "Assume that the final peak represents the time of coalescence, which would be given by `t[-1]` (the `[-1]` selects the last value in the list).\n",
    "Construct an array of \"time before coalescence\" by subtracting the first 5 peak times from the final peak time `t[-1]`. Call this new array `dt`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### What we want to code (part 2):\n",
    "Find the instantaneous period, `T` by finding the time difference between each pair of adjacent peaks.\n",
    "You can use the `numpy.diff()` function in numpy to find the difference between the peak times. \n",
    "Try to write your own function that gives you the same result as `numpy.diff()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 1:\n",
    "In both cases, your new arrays `dt` and `T` will have 5 entries instead of 6 entries.\n",
    "Complete the code below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt =\n",
    "T =\n",
    "\n",
    "### Your code here ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Part B\n",
    "\n",
    "#### What we want to code:\n",
    "Next we want to make a function that gives us the chirp mass from the two arrays `dt` and `T` that you made in Part A. \n",
    "The function will return another array with **5** estimates of the chirp mass since there are 5 entries for each array. You will use the equation $\\mathcal{M} = 3000 \\, T^{8/5} \\Delta T^{-3/5}$. **Make a function called `chirpmass` that takes arguments `T` and `dt`.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 1:\n",
    "Complete the code below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def chirpmass(T, dt):\n",
    "    ### Your code here ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What we want to code:\n",
    "Use the function you defined above to compute an array of chirpmasses `M` from your imput arrays, `dt` and `T`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 2:\n",
    "Complete the code below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = ### Your code here ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Part C\n",
    "\n",
    "#### More details before you start working:\n",
    "GW150914 has only one chirp mass, but each of these points gives us an estimate.  We should interpret the result as 5 estimates of its chirpmass with uncertainties caused both by the LIGO data **and** by our measurement using the plot. We can take an average or mean value to get our best estimate. \n",
    "\n",
    "#### What we want to code:\n",
    "Compute the mean value of the chirp mass estimates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hint 1:\n",
    "Numpy provides a function called `numpy.mean` to calculate this. Use it here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Your code here ###\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Part D\n",
    "Finally, compare the mean value of chirp mass estimates that you got in Part C to the estimate in [this paper](https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.116.061102)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:igwn-38]",
   "language": "python",
   "name": "conda-env-igwn-38-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
