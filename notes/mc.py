import numpy

def mc(dt, T):
    return 0.015 * T**(8/5.) * dt**(-.6)

mc1 = mc(.076, .0215)
mc2 = mc(.0545, .0188)
mc3 = mc(.0357, .0172)
mc4 = mc(.0185, .0115)
mc5 = mc(.0069, .0069)


mcs = (mc1, mc2, mc3, mc4, mc5)

for mc in mcs:
    print (round(mc, 6))

print (round(numpy.mean(mcs), 6))
