# PSU GW Camp 2021

Zoom link: https://psu.zoom.us/j/92041796269
623481

## Agenda

### Day 1
**Command to retreive jupyter notebooks** `cp -r /shared/psu-gw-camp-2021/jupyter-python/day1 ~`


| time 	| topic	 |
| ------| ------ |
| 10:00	| Introductions and welcome |
| 10:30	| [The history of Gravitational Wave physics and Astronomy](https://git.ligo.org/chad-hanna/psu-gw-camp-2021/-/blob/master/talks/day1/Day1_talk1_history.pdf) |
| 11:00	| [The current state of Gravitational Wave Physics and Astronomy](https://docs.google.com/presentation/d/1rxJ-hJ_TH8Qma4GBvlXcAE2aPACNulbY48LpE-8Mn6k/edit#slide=id.p)	|
| 11:30	| [The future of Gravitational Wave Physics and Astronomy](https://docs.google.com/presentation/d/1dhxo3Ly5bZ25KoJ6IifHO3CuGLXeWlZVtjGdpmYhioA/edit?usp=sharing) |
| 12:00	| Lunch	- Everyone is on their own. |
| 1:30	| Introduction to Jupyter, Python, and Data Analysis onboarding	|
| 2:45	| Description of HW assignment and first day close-out |


### Day 2
**Command to retreive jupyter notebooks** `cp -r /shared/psu-gw-camp-2021/jupyter-python/day2 ~`

| time  | topic |
| ----- | ----- |
| 10:00	| [Basics of LIGO data and signal processing](https://docs.google.com/presentation/d/1nOO8kWcVTIb7Tfb4MIIGODXyzTCK_ZVFLf2mVZ3qMjM/edit#slide=id.ge0272d0858_0_61) |
| 11:00	| Basics of signal processing |
| 12:00	| Lunch	- Everyone is on their own. |
| 1:30	| Hands on GWOSC Tutorials |
| 2:45	| Description of HW assignment and first day close-out |

### Day 3
**Command to retreive jupyter notebooks** `cp -r /shared/psu-gw-camp-2021/jupyter-python/day3 ~`

| time  | topic |
| ----- | ----- |
| 10:00	| [GW signals, modeling, waveforms, and inference](https://docs.google.com/presentation/d/1tg_09AYNl9vz8RppuHpzg5WBA12RY_wdewXnI4sZ7qA/edit?usp=sharing) |
| 12:00	| Lunch	- Everyone is on their own. |
| 1:30	| Hands on measuring by hand the chirp mass of binary waveforms |
| 2:45	| Close-out	|



